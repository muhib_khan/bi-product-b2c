package com.onebill.productapp.constant;

public final class AppConstants {

	private AppConstants() {
	}
	
	public static final String ACTIVE ="active";
	public static final String ID = "id";
	public static final String START = "start";
	public static final String END ="end";
	public static final String NAME ="name";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String ASTERISK = "*";
	
}

package com.onebill.productapp.constant;

public final class URIConstants {
	
	private URIConstants() {
	}

	public static final String CUSTOMER_URI="/customers/";
	public static final String CUSTOMER_WITH_ID_URI="/customer/{id}";
	public static final String ADDRESS_URI = CUSTOMER_WITH_ID_URI+"/address";
	public static final String ADDRESS_WITH_IDS_URI = CUSTOMER_WITH_ID_URI+"/address/{aid}";
	public static final String PRODUCT_URI = "/product";
	public static final String PRODUCT_WITH_URI = "/product/{id}";
	public static final String PRODUCT_WITH_PRICE_URI = PRODUCT_URI + "/price/{start}/{end}";
	public static final String PRODUCT_WITH_RATING_URI =PRODUCT_URI + "/rating/{start}/{end}";
	public static final String PRODUCT_NAME = PRODUCT_URI+"/{name}";
	public static final String REVIEW_URI = "/review";
	public static final String ORDER_URI = "/order";
}

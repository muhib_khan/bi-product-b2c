package com.onebill.productapp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onebill.productapp.dto.AddressBean;
import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.service.AddressService;

import static com.onebill.productapp.constant.AppConstants.ASTERISK;
import static com.onebill.productapp.constant.AppConstants.TRUE;
import static com.onebill.productapp.constant.URIConstants.*;

@CrossOrigin(origins = ASTERISK, allowCredentials = TRUE)
@RestController
@RequestMapping(value = "/api/rest/address")
public class AddressController {

	@Autowired
	AddressService service;
	
	@PostMapping(value=ADDRESS_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse addAddress(@Valid @RequestBody AddressBean bean, @PathVariable int id) {
		return service.addAddress(id,bean);
	}
	@GetMapping(value=ADDRESS_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse getAddress(@Valid @PathVariable int id) {
		return service.getAddress(id);
	}
	@PutMapping(value=ADDRESS_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse updateAddress(@Valid @RequestBody AddressBean bean, @PathVariable int id) {
		return service.addAddress(id,bean);
	}
	@DeleteMapping(value = ADDRESS_WITH_IDS_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse getAddress(@PathVariable int id,@PathVariable int aid) {
		return service.deleteAddress(id,aid);
	}
}

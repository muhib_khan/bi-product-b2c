package com.onebill.productapp.controller;

import static com.onebill.productapp.constant.AppConstants.ACTIVE;
import static com.onebill.productapp.constant.AppConstants.ASTERISK;
import static com.onebill.productapp.constant.AppConstants.TRUE;
import static com.onebill.productapp.constant.URIConstants.CUSTOMER_URI;
import static com.onebill.productapp.constant.URIConstants.CUSTOMER_WITH_ID_URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onebill.productapp.dto.CustomerBean;
import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.service.CustomerService;

@CrossOrigin(origins = ASTERISK, allowCredentials = TRUE)
@RestController
@RequestMapping(value = "/api/rest")
public class CustomerController {

	@Autowired
	CustomerService service;

	@PostMapping(value =CUSTOMER_URI, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse addCustomer(@Valid @RequestBody CustomerBean bean) {
		return service.addCustomer(bean);
	}
	@PutMapping(value =CUSTOMER_URI, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse updateCustomer(@Valid @RequestBody CustomerBean bean) {
		return service.updateCustomer(bean);
	}


	@PutMapping(value = CUSTOMER_WITH_ID_URI,produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse deleteCustomer(@PathVariable Integer id) {
		return service.deleteCustomer(id);
	}


	@GetMapping(value = CUSTOMER_URI,params = ACTIVE, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse getActiveCustomer(boolean active) {
		return service.getActiveCustomer(active);
	}


	@GetMapping(value =CUSTOMER_WITH_ID_URI, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse getCustomer(@Valid @PathVariable int id) {
		return service.getCustomer(id);
	}


	@GetMapping(value = CUSTOMER_URI, produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse getAllCustomer() {
		return service.getAllCustomer();
	}
}

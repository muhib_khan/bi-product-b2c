package com.onebill.productapp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onebill.productapp.dto.OrderBean;
import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.service.OrderService;
import static com.onebill.productapp.constant.URIConstants.*;
import static com.onebill.productapp.constant.AppConstants.*;


@CrossOrigin(origins = ASTERISK, allowCredentials = TRUE)
@RestController
@RequestMapping(value = "/api/rest")
public class OrderController {

	@Autowired
	OrderService service;
	
	@PostMapping(value=ORDER_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse placeOrder(@Valid @RequestBody OrderBean bean) {
		return service.placeOrder(bean);
	}
	@GetMapping(value=ORDER_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse updateCustomer() {
		return service.getOrder();
	}
}

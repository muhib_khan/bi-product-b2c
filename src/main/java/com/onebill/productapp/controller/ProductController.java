package com.onebill.productapp.controller;

import static com.onebill.productapp.constant.URIConstants.*;
import static com.onebill.productapp.constant.AppConstants.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.dto.ProductBean;
import com.onebill.productapp.service.ProductService;

@CrossOrigin(origins = ASTERISK, allowCredentials = TRUE)
@RestController
@RequestMapping(value = "/api/rest")
public class ProductController {

	@Autowired
	ProductService service;
	
	@PostMapping(value=PRODUCT_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse addProduct(@Valid @RequestBody ProductBean bean) {
		return service.addProduct(bean);
	}
	@PutMapping(value=PRODUCT_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse updateProduct(@Valid @RequestBody ProductBean bean) {
		return service.editProduct(bean);
	}
	@PutMapping(value=PRODUCT_WITH_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse deleteProduct(@PathVariable(value = ID) int productId) {
		return service.deleteProduct(productId);
	}
	@GetMapping(value=PRODUCT_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse getProduct() {
		return service.getAllProduct();
	}
	@GetMapping(value=PRODUCT_URI, params = ACTIVE,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse getActiveProduct(@PathParam("active")boolean active) {
		return service.getActiveProduct( active);
	}
	@GetMapping(value=PRODUCT_WITH_PRICE_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse getPriceBetween(@PathVariable(value = START) double start,@PathVariable(value = END) double end) {
		return service.getProductByPriceRange(start, end);
	}
	@GetMapping(value=PRODUCT_WITH_RATING_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse getRatingBetween(@PathVariable(value = START) double start,@PathVariable(value = END) double end) {
		return service.getProductByRatingRange(start, end);
	}
	@GetMapping(value=PRODUCT_NAME,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse getProductNameLike(@PathVariable(value = NAME) String name) {
		return service.getProductByNameLike(name);
	}
	
}

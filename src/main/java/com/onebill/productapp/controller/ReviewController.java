package com.onebill.productapp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.dto.ReviewBean;
import com.onebill.productapp.service.ReviewService;

import static com.onebill.productapp.constant.URIConstants.*;
import static com.onebill.productapp.constant.AppConstants.*;

@CrossOrigin(origins = ASTERISK, allowCredentials = TRUE)
@RestController
@RequestMapping(value = "/api/rest")
public class ReviewController {

	@Autowired
	ReviewService service;
	
	@PostMapping(value=REVIEW_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Object> addReview(@Valid @RequestBody ReviewBean bean) {
		return new ResponseEntity<Object>(service.addReview(bean),HttpStatus.OK);
//		return service.addReview(bean);
	}
	@DeleteMapping(value=REVIEW_URI,produces=MediaType.APPLICATION_JSON_VALUE )
	public GenericResponse deleteReview(@Valid @RequestParam Integer bean) {
		return service.removeReview(bean);
	}
	@GetMapping(value=REVIEW_URI,produces=MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse getReview() {
	return service.getReview();
	}
}

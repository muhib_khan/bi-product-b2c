package com.onebill.productapp.dto;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "address")
public class AddressBean implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "address_id", length = 15)
	private int addressId;
	
	@Min(value = 6)
	@Max(value = 6)
	@Column(name = "pincode", length = 15)
	private long pincode;
	
	@Size(max=30,min=1,message="country should not exceed 255 characters")
	@Column(name = "country", length = 30)
	private String country;
	
	@Size(max=30,min=1,message="state should not exceed 255 characters")
	@Column(name = "state", length = 30)
	private String state;
	
	@Size(max=30,min=1,message="city should not exceed 255 characters")
	@Column(name = "city", length = 30)
	private String city;
	
	@Size(max=255,min=1,message="address should not exceed 255 characters")
	@Column(name = "address", length = 255)
	private String address;
	
	@Size(min=4,max=10,message="address type should be greater than 4 and less than 10 characters")
	@Column(name="address_type", length=10)
	private String addressType;
	
//	@Getter(value=AccessLevel.NONE)
	@JsonIgnore
	@ManyToOne(cascade = CascadeType.ALL)
	private CustomerBean customer;
}

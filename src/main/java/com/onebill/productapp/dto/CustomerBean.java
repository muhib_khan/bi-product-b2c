package com.onebill.productapp.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@Entity
@Table(name = "customer")
public class CustomerBean implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "customer_id", length = 10)
	private int customerId;
	@NotNull
	@Size(max = 20, min = 3, message = "length should be greater than 3 and less than 20")
	@Column(name = "first_name", length = 20)
	private String firstName;
	@NotNull
	@Size(max = 20, min = 1, message = "length should be greater than 3 and less than 20")
	@Column(name = "last_name", length = 20)
	private String lastName;
	@NotNull
	@Email
	@Column(name = "email", length = 30, unique = true)
	private String email;
	@NotNull
	@Size(max = 15, min = 10, message = "length should be equal to 10 or less than 15")
	@Column(name = "phone", length = 15, unique = true)
	private String phone;
	@NotNull
	@Column(name = "is_active")
	private boolean isActive;
	@Column(name = "customer_type", length = 10)
	private String customerType;

	@OneToMany(mappedBy = "customer")
	private List<AddressBean> addressBean;

	@JsonIgnore
	@OneToMany(mappedBy = "customerBean")
	private List<OrderBean> orderBeans;
	
//	@OneToOne(cascade= CascadeType.ALL, mappedBy = "customerBeans")
//	private ReviewBean reviewBean;

}

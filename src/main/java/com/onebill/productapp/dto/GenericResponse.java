package com.onebill.productapp.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Scope("prototype")
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResponse {
	private String message;
	private Object data;

}

package com.onebill.productapp.dto;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "order_product")
public class OrderBean implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "order_id")
	private int orderId;

//	@Size(max = 255,min = 2,message="address should not exceed 255 characters")
//	@Column(name = "shipping_address",length=255)
//	private String shippingAddress;

	@NotNull
	@CreationTimestamp
	@Column(name = "date_of_order")
	private LocalDate dateOfOrder;
	
	
//	@ElementCollection
//	private List<ProductBean> listOfProducts;
	
	@NotNull
	@Max(value=10,message="maximum quantity 10")
	@Min(value=1,message="minimum quantity 1")
	@Column(name = "quantity",length=10)
	private int quantity;
	
	@Digits(integer=6, fraction=2)
	@Column(name = "price")
	private double price;

	@NotNull
	@ManyToOne(cascade = CascadeType.ALL)
	private CustomerBean customerBean;
	
	@NotNull
	@ManyToOne(cascade = CascadeType.ALL)
	private  ProductBean productBean;
	
	@OneToOne(cascade = CascadeType.ALL)
	private AddressBean addressBean;
	
//	@OneToOne(cascade= CascadeType.ALL)
//	private ReviewBean reviewBean;
	
}

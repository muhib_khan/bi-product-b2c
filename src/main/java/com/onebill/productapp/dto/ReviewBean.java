package com.onebill.productapp.dto;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
@Table(name = "review")
public class ReviewBean implements Serializable {

	 @Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "review_id")
	private Integer reviewId;

	@Size(max = 255, min = 1, message = "review should be null and not exceed 255 characters")
	@Column(name = "review", length = 255)
	private String review;

	@ManyToOne()
	private CustomerBean customerBeans;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private ProductBean productBeans;
	
//	@OneToOne(cascade = CascadeType.ALL,mappedBy = "reviewBean")
//	private OrderBean orderBean;

}

package com.onebill.productapp.exception;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.onebill.productapp.util.ResponseUtil;

@RestControllerAdvice
public class CustomizedExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	ExceptionResponse exResponse;

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllException(Exception ex, WebRequest request) {

		return setException(ex, request, HttpStatus.ACCEPTED);
	}

	@ExceptionHandler(CustomerNotFoundException.class)
	public final ResponseEntity<Object> handleCustomerNotFoundException(CustomerNotFoundException ex, WebRequest request) {
		return setException(ex, request, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ProductNotFoundExecption.class)
	public final ResponseEntity<Object> handleProductNotFoundException(ProductNotFoundExecption ex, WebRequest request) {
		return setException(ex, request, HttpStatus.NOT_FOUND);
	}
	

	private ResponseEntity<Object> setException(Exception ex, WebRequest request, HttpStatus status) {
		exResponse.setDetails(request.getDescription(false));
		exResponse.setMessage(ex.getLocalizedMessage());
		exResponse.setTimestamp(new Date());
		return new ResponseEntity<>(ResponseUtil.fillerException("404 NOT FOUND", exResponse), status);
	}
}

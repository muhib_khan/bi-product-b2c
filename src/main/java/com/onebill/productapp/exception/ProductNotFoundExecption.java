package com.onebill.productapp.exception;

public class ProductNotFoundExecption extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public ProductNotFoundExecption(String message) {
		super(message);
	}
	

}

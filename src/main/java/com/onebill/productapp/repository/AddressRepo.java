package com.onebill.productapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onebill.productapp.dto.AddressBean;

public interface AddressRepo extends JpaRepository<AddressBean, Integer> {
	
}

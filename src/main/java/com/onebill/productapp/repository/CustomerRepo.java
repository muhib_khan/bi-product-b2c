package com.onebill.productapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onebill.productapp.dto.CustomerBean;

public interface CustomerRepo extends JpaRepository<CustomerBean, Integer> {

	boolean existsByEmail(String email);
	
	List<CustomerBean> findByIsActiveIs(boolean active);
	
	List<CustomerBean> findByEmailEquals(String email);
	
}

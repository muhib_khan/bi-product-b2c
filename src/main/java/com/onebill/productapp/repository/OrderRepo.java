package com.onebill.productapp.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.onebill.productapp.dto.OrderBean;

public interface OrderRepo extends PagingAndSortingRepository<OrderBean, Integer> {

//	@Query(value="select count(*) from customer where customer_id=:cid",nativeQuery=true)
//	Integer existByCustomerId(@Param("cid")Integer cid);
}

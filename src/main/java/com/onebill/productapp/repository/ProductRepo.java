package com.onebill.productapp.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.onebill.productapp.dto.ProductBean;

public interface ProductRepo extends PagingAndSortingRepository<ProductBean, Integer> {


	List<ProductBean> findByIsActiveIs(boolean active,Pageable pageable);

	boolean existsByProductCode(String pid);

	List<ProductBean> findByPriceBetweenAndIsActiveTrue(double start, double end, Pageable pageable);

	List<ProductBean> findByRatingBetweenAndIsActiveTrue(double start, double end, Pageable pageable);

	List<ProductBean> findByProductNameStartingWithAndIsActiveTrue(String name,Pageable pageable);
	
	
	
}

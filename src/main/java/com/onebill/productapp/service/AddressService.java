package com.onebill.productapp.service;

import com.onebill.productapp.dto.AddressBean;
import com.onebill.productapp.dto.GenericResponse;

public interface AddressService {

	GenericResponse addAddress(int id,AddressBean address);
	
	GenericResponse getAddress(int addressId);
	
	GenericResponse updateAddress(int id,AddressBean address);
	
	GenericResponse deleteAddress(int customerId,int addressId);
}

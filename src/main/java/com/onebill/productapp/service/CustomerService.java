package com.onebill.productapp.service;


import com.onebill.productapp.dto.CustomerBean;
import com.onebill.productapp.dto.GenericResponse;

public interface CustomerService {

	GenericResponse addCustomer(CustomerBean customer);
	
	GenericResponse updateCustomer(CustomerBean customer);
	
	GenericResponse deleteCustomer(int id);
	
	GenericResponse getActiveCustomer(boolean active);
	
	GenericResponse getAllCustomer();

	GenericResponse getCustomer(int id);

}

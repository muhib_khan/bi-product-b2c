package com.onebill.productapp.service;


import com.onebill.productapp.dto.OrderBean;
import com.onebill.productapp.dto.GenericResponse;

public interface OrderService {

	GenericResponse placeOrder(OrderBean order);
	
	GenericResponse getOrder();
	
}

package com.onebill.productapp.service;


import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.dto.ProductBean;

public interface ProductService {

	GenericResponse addProduct(ProductBean product);

	GenericResponse editProduct(ProductBean product);

	GenericResponse deleteProduct(Integer productId);

	GenericResponse getAllProduct();

	GenericResponse getActiveProduct(boolean active );
	
	GenericResponse getProductByPriceRange(double start, double end);
	
	GenericResponse getProductByRatingRange(double start, double end);
	
	GenericResponse getProductByNameLike(String name);
}

package com.onebill.productapp.service;

import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.dto.ReviewBean;

public interface ReviewService {

	GenericResponse addReview(ReviewBean review);

	GenericResponse removeReview(int reviewId);

	GenericResponse getReview();
}

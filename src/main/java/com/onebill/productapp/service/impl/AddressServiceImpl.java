package com.onebill.productapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onebill.productapp.dto.AddressBean;
import com.onebill.productapp.dto.CustomerBean;
import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.exception.CustomerNotFoundException;
import com.onebill.productapp.repository.AddressRepo;
import com.onebill.productapp.repository.CustomerRepo;
import com.onebill.productapp.service.AddressService;
import com.onebill.productapp.util.ResponseUtil;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	AddressRepo addressRepo;
	
	@Autowired
	CustomerRepo customerRepo;

	@Override
	public GenericResponse addAddress(int id,AddressBean address) {
			CustomerBean customer = customerRepo.findById(id).orElseThrow(() -> new CustomerNotFoundException("Customer with id '"+id+"' doesnt exists"));
			if (customer != null && address != null) {
				address.setCustomer(customer);
				addressRepo.save(address);
				return ResponseUtil.fillerSuccess("address added successfully", address);
			} else {
				return ResponseUtil.fillerFailure("cid doesnt exist", null);
			}
	}

	@Override
	public GenericResponse getAddress(int id) {
		CustomerBean findByCustomerId = customerRepo.findById(id).orElseThrow(() -> new CustomerNotFoundException("Customer with id '"+id+"' doesnt exists"));
		return ResponseUtil.fillerSuccess("address retrived successfully", findByCustomerId);
	}

	@Override
	public GenericResponse updateAddress(int id, AddressBean address) {
		try {
			if (customerRepo.existsById(id)) {
				AddressBean existingAddress = addressRepo.findById(address.getAddressId()).get();
				existingAddress.setCustomer(customerRepo.findById(id).get());
				existingAddress.setAddressType(address.getAddress());
				existingAddress.setAddress(address.getAddress());
				addressRepo.save(existingAddress);
				return ResponseUtil.fillerSuccess("address updated successfully", address);
			} else {
				return ResponseUtil.fillerFailure("cid doesnt exist", null);
			}
		} catch (Exception e) {
			return ResponseUtil.fillerFailure("Exception occured :-", e.getLocalizedMessage());
		}
	}

	@Override
	public GenericResponse deleteAddress(int customerId,int addressId) {
		try {
			if (customerRepo.existsById(customerId) && addressRepo.existsById(addressId)) {
				addressRepo.deleteById(addressId);
				return ResponseUtil.fillerSuccess("address deleted successfully", addressId);
			} else {
				return ResponseUtil.fillerFailure("id doesnt exists", null);
			}
		} catch (Exception e) {
			return ResponseUtil.fillerException("Exception occured :-", e.getLocalizedMessage());
		}
	}
}

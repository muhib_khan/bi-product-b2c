package com.onebill.productapp.service.impl;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onebill.productapp.dto.CustomerBean;
import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.exception.CustomerNotFoundException;
import com.onebill.productapp.repository.CustomerRepo;
import com.onebill.productapp.service.CustomerService;
import com.onebill.productapp.util.DynamicResponseFilter;
import com.onebill.productapp.util.ResponseUtil;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepo customerRepo;

	@Override
	public GenericResponse addCustomer(CustomerBean customer) {
		try {
			if (!customerRepo.existsByEmail(customer.getEmail())) {
				customer.setActive(true);
				CustomerBean save = customerRepo.save(customer);
				return ResponseUtil.fillerSuccess("customer added successfully", save);
			} else {
				return ResponseUtil.fillerFailure("customer email already exist", null);
			}
		} catch (Exception e) {
			return ResponseUtil.fillerException("Exception occured :-", e.getMessage());
		}

	}

	@Override
	public GenericResponse updateCustomer(CustomerBean customer) {
		try {
			if (customerRepo.existsById(customer.getCustomerId())) {

				CustomerBean customerBean = customerRepo.findById(customer.getCustomerId())
						.orElseThrow(RuntimeException::new);
				customerBean.setCustomerType(customer.getCustomerType());
				customerBean.setEmail(customer.getEmail());
				customerBean.setFirstName(customer.getFirstName());
				customerBean.setLastName(customer.getLastName());
				customerBean.setPhone(customer.getPhone());
				CustomerBean save = customerRepo.save(customerBean);
				return ResponseUtil.fillerSuccess("customer updated successfully", save);
			} else {
				return ResponseUtil.fillerFailure("data not found for registered Customer ", null);
			}
		} catch (Exception e) {
			return ResponseUtil.fillerException("Exception occured :-", e.getMessage());
		}
	}

	@Override
	public GenericResponse deleteCustomer(int customerId) {
		CustomerBean customerBean = customerRepo.findById(customerId).orElseThrow(() -> new CustomerNotFoundException("Customer with id '"+customerId+"' doesnt exists"));
				customerBean.setActive(false);
				CustomerBean save = customerRepo.save(customerBean);
				return ResponseUtil.fillerSuccess("customer deleted successfully", save);
	}

	@Override
	public GenericResponse getActiveCustomer(boolean active) {
		try {
			List<CustomerBean> activeCustomer = customerRepo.findByIsActiveIs(active);
			if (!activeCustomer.isEmpty()) {
				return ResponseUtil.fillerSuccess("All Active Customers found", activeCustomer);
			} else {
				return ResponseUtil.fillerFailure("No Active Customers found", null);
			}
		} catch (Exception e) {
			return ResponseUtil.fillerException("Exception occured :-", e.getLocalizedMessage());
		}
	}

	@Override
	public GenericResponse getAllCustomer() {
		try {
			return ResponseUtil.fillerSuccess("Customers found", customerRepo.findAll());
		} catch (Exception e) {
			return ResponseUtil.fillerException("Exception occured :-", e.getLocalizedMessage());
		}
	}

	//no working under test
	@Override
	public GenericResponse getCustomer(int id) {
		CustomerBean customerBeans = customerRepo.findById(id).orElseThrow(() -> new CustomerNotFoundException("Customer with id '"+id+"' doesnt exists"));
		Set<String> filteringFields = new LinkedHashSet<>(Arrays.asList("firstName","lastName","addressBean"));
//		return DynamicResponseFilter.dynamicMappingFilter(customerBeans, filteringFields, "CustomerBeanId");
		return ResponseUtil.fillerSuccess("Customer found", DynamicResponseFilter.dynamicMappingFilter(customerBeans, filteringFields, "CustomerBeanId"));
	}

}

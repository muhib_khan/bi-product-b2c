package com.onebill.productapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onebill.productapp.dto.AddressBean;
import com.onebill.productapp.dto.CustomerBean;
import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.dto.OrderBean;
import com.onebill.productapp.dto.ProductBean;
import com.onebill.productapp.repository.AddressRepo;
import com.onebill.productapp.repository.CustomerRepo;
import com.onebill.productapp.repository.OrderRepo;
import com.onebill.productapp.repository.ProductRepo;
import com.onebill.productapp.service.OrderService;
import com.onebill.productapp.util.ResponseUtil;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepo orderRepo;

	@Autowired
	AddressRepo addressRepo;

	@Autowired
	CustomerRepo customerRepo;

	@Autowired
	ProductRepo productRepo;


	@Override
	public GenericResponse placeOrder(OrderBean order) {
		GenericResponse response = null;
		OrderBean o = new OrderBean();

		try {
			if (order.getAddressBean() !=null && order.getCustomerBean() !=null && order.getProductBean() !=null ) {
					if (productRepo.existsById(order.getProductBean().getProductId())) {
						int remainingQuantity = Math.subtractExact(productRepo.findById(order.getProductBean().getProductId()).get().getQuantity(), order.getQuantity());
						if (remainingQuantity >=0 && customerRepo.existsByEmail(order.getCustomerBean().getEmail())) {
							ProductBean product = productRepo.findById(order.getProductBean().getProductId()).get();
							product.setQuantity(remainingQuantity);
							 CustomerBean customer = customerRepo.findById(order.getCustomerBean().getCustomerId()).get();
							 if (addressRepo.existsById(order.getAddressBean().getAddressId())) {
							AddressBean address = addressRepo.findById(order.getAddressBean().getAddressId()).get();
							o.setAddressBean(address);
							}
							
							o.setCustomerBean(customer);
							o.setDateOfOrder(order.getDateOfOrder());
							o.setPrice(order.getPrice());
							o.setProductBean(product);
							o.setQuantity(order.getQuantity());
							orderRepo.save(o);
							response = ResponseUtil.fillerSuccess("Order Placed Succussfuly", o);
							
						}
						else {
							response = ResponseUtil.fillerFailure("Order not placed", null);
						}
					}else {
						response = ResponseUtil.fillerFailure("Order not placed", null);
					}

			}
		}
		catch (Exception e) {
			response = ResponseUtil.fillerException("Exceptin occured:-", e.getLocalizedMessage());
		}
		return response;
	}
	
//	@Transactional
//	@Override
//	public GenericResponse placeOrder(OrderBean order) {
//		GenericResponse response = null;
//		OrderBean o = new OrderBean();
//
//		try {
//			if (order.getAddressBean() != null && order.getCustomerBean() != null && order.getProductBean() != null ) {
//				ProductBean product = productRepo.findById(order.getProductBean().getProductId()).get();
//				int remainingQuantity = Math.subtractExact(product.getQuantity(), order.getQuantity());
// 							if (remainingQuantity >=0) {
//							product.setQuantity(remainingQuantity);
//							CustomerBean customer = customerRepo.findById(order.getCustomerBean().getCustomerId()).get();
//							AddressBean address = addressRepo.findById(order.getAddressBean().getAddressId()).get();
//							o.setCustomerBean(customer);
//							o.setProductBean(product);
//							o.setAddressBean(address);
//							o.setDateOfOrder(order.getDateOfOrder());
//							o.setPrice(order.getPrice());
//							o.setQuantity(order.getQuantity());
//							orderRepo.save(o);
//							response = ResponseUtil.fillerSuccess("Order Placed Succussfuly", o);
//							
//						}
//						else {
//							response = ResponseUtil.fillerFailure("Order not placed", null);
//						}
//					}
//		else {
//						response = ResponseUtil.fillerFailure("Order not placed", null);
//					}
//
//			
//		}
//		catch (Exception e) {
//			response = ResponseUtil.fillerException("Exceptin occured:-", e.getLocalizedMessage());
//		}
//		return response;
//	}

	@Override
	public GenericResponse getOrder() {
		GenericResponse response = null;

		try {
			response = ResponseUtil.fillerSuccess("Order found", orderRepo.findAll());
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e);
		}
		return response;
	}
}

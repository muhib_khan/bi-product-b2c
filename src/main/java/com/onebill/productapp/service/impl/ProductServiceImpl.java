package com.onebill.productapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.dto.ProductBean;
import com.onebill.productapp.exception.ProductNotFoundExecption;
import com.onebill.productapp.repository.ProductRepo;
import com.onebill.productapp.service.ProductService;
import com.onebill.productapp.util.ResponseUtil;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepo productRepo;

	@Override
	public GenericResponse addProduct(ProductBean product) {
		GenericResponse response = null;
		try {
			if (!productRepo.existsByProductCode(product.getProductCode())) {
				product.setActive(true);
				ProductBean save = productRepo.save(product);
				response = ResponseUtil.fillerSuccess("product added successfully", save);
			} else {
				response = ResponseUtil.fillerFailure("product Code is not unique", null);
			}
		} catch (Exception e) {
//			throw new NullPointerException();
			response = ResponseUtil.fillerException("Exception occured :-", e.getMessage());
		}
		return response;
	}

	@Override
	public GenericResponse editProduct(ProductBean product) {
		GenericResponse response = null;
		try {
			if (productRepo.existsById(product.getProductId())) {

				ProductBean bean = productRepo.findById(product.getProductId())
						.orElseThrow(() -> new ProductNotFoundExecption(
								"Product with id '" + product.getProductId() + "' doesnt exists"));
				bean.setProductName(product.getProductName());
				bean.setProductCode(product.getProductCode());
				bean.setPrice(product.getPrice());
				bean.setQuantity(product.getQuantity());
				bean.setDescription(product.getDescription());
				bean.setRating(product.getRating());

				ProductBean save = productRepo.save(bean);
				response = ResponseUtil.fillerSuccess("product updated successfully", save);
			} else {
				response = ResponseUtil.fillerFailure("product not found", null);
			}
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e.getMessage());
		}
		return response;
	}

	@Override
	public GenericResponse deleteProduct(Integer productId) {
		GenericResponse response = null;
		try {
			if (productRepo.existsById(productId)) {

				ProductBean bean = productRepo.findById(productId).orElseThrow(
						() -> new ProductNotFoundExecption("Product with id '" + productId + "' doesnt exists"));
				bean.setActive(false);
				productRepo.save(bean);
				response = ResponseUtil.fillerSuccess("product deleted successfully", null);
			} else {
				response = ResponseUtil.fillerFailure("product not found", null);
			}
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e.getMessage());
		}
		return response;
	}

	@Override
	public GenericResponse getAllProduct() {
		GenericResponse response = null;
		

		try {
			response = ResponseUtil.fillerSuccess("Product found",
					productRepo.findAll(PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "productName"))).getContent());
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e.getMessage());
		}
		return response;
	}

	@Override
	public GenericResponse getActiveProduct(boolean active) {
		GenericResponse response = null;

		try {
			List<ProductBean> productBeans = productRepo
					.findByIsActiveIs(active,PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "productName")));
			if (!productBeans.isEmpty()) {
				response = ResponseUtil.fillerSuccess("All Active product found", productBeans);
			} else {
				response = ResponseUtil.fillerFailure("No Active product found", null);
			}
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e.getMessage());
		}
		return response;
	}

	@Override
	public GenericResponse getProductByPriceRange(double start, double end) {
		GenericResponse response = null;
		try {

			List<ProductBean> productBeans = productRepo.findByPriceBetweenAndIsActiveTrue(start, end,
					PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "price")));
			if (!productBeans.isEmpty()) {
				response = ResponseUtil.fillerSuccess("product found Range", productBeans);
			} else {
				response = ResponseUtil.fillerFailure("product not in a found", null);
			}
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e.getMessage());
		}
		return response;
	}

	@Override
	public GenericResponse getProductByRatingRange(double start, double end) {
		GenericResponse response = null;
		try {

			List<ProductBean> productBeans = productRepo.findByRatingBetweenAndIsActiveTrue(start, end,
					PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "rating")));
			if (!productBeans.isEmpty()) {
				response = ResponseUtil.fillerSuccess("product found Range", productBeans);
			} else {
				response = ResponseUtil.fillerFailure("product not in a found", null);
			}
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e.getMessage());
		}
		return response;

	}

	@Override
	public GenericResponse getProductByNameLike(String name) {
		GenericResponse response = null;
		try {

			List<ProductBean> productBeans = productRepo.findByProductNameStartingWithAndIsActiveTrue(name,
					PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "productName")));
			if (!productBeans.isEmpty()) {
				response = ResponseUtil.fillerSuccess("product found Range", productBeans);
			} else {
				response = ResponseUtil.fillerFailure("product not in a found", null);
			}
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e.getMessage());
		}
		return response;

	}

}

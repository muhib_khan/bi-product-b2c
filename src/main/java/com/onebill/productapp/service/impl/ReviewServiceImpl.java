package com.onebill.productapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.dto.ReviewBean;
import com.onebill.productapp.repository.CustomerRepo;
import com.onebill.productapp.repository.ReviewRepo;
import com.onebill.productapp.service.ReviewService;
import com.onebill.productapp.util.ResponseUtil;

@Service
public class ReviewServiceImpl implements ReviewService {

	@Autowired
	ReviewRepo reviewRepo;

	@Autowired
	CustomerRepo customerRepo;

	@Override
	public GenericResponse addReview(ReviewBean review) {

		GenericResponse response = null;
		try {
			if (customerRepo.findById(review.getCustomerBeans().getCustomerId()) != null) {
				ReviewBean save = reviewRepo.save(review);
				response = ResponseUtil.fillerSuccess("review added successfully", save);
			} else {
				response = ResponseUtil.fillerFailure("review id already exist ", null);
			}
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e.getLocalizedMessage());
		}
		return response;
	}

	@Override
	public GenericResponse removeReview(int reviewId) {
		GenericResponse response = null;
		try {
			if (reviewRepo.existsById(reviewId)) {
				reviewRepo.deleteById(reviewId);
				response = ResponseUtil.fillerSuccess("review removed successfully", null);
			} else {
				response = ResponseUtil.fillerFailure("review not found", null);
			}
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e.getLocalizedMessage());
		}
		return response;
	}

	@Override
	public GenericResponse getReview() {
		GenericResponse response = null;

		try {
			response = ResponseUtil.fillerSuccess("Review found", reviewRepo.findAll(PageRequest.of(0, 3,Sort.by(Sort.Direction.DESC))));
		} catch (Exception e) {
			response = ResponseUtil.fillerException("Exception occured :-", e.getLocalizedMessage());
		}
		return response;
	}
}

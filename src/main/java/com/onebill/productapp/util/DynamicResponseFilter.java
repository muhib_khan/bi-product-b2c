package com.onebill.productapp.util;

import java.util.Set;

import org.springframework.http.converter.json.MappingJacksonValue;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

public class DynamicResponseFilter {

	private DynamicResponseFilter() {
	}

	public static MappingJacksonValue dynamicMappingFilter(Object someBean, Set<String> filteringFields,
			String beanId) {
		// step 1: create MappingJacksonValue pass bean
		MappingJacksonValue mapping = new MappingJacksonValue(someBean);
		// step 2: create filter then pass fields you want to pass
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept(filteringFields);
		// step 3: use FilterProvider then pass FilterID( annotate on bean class using
		// @JsonFilter(value = "SomeBeanDynamicFilteringId")) and filter
		FilterProvider filters = new SimpleFilterProvider().addFilter(beanId, filter);
		// set filter to MappingJacksonValue
		mapping.setFilters(filters);
		// filtered MappingJacksonValue
		return mapping;
	}
}

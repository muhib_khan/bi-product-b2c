package com.onebill.productapp.util;

import org.springframework.stereotype.Component;

import com.onebill.productapp.dto.GenericResponse;

@Component
public class ResponseUtil {

	private ResponseUtil() {
	}

	public static GenericResponse fillerSuccess(String message, Object data) {
		return GenericResponse.builder().message(message).data(data).build();
	}

	public static GenericResponse fillerFailure(String message, Object data) {
		return GenericResponse.builder().message(message).data(data).build();
	}

	public static GenericResponse fillerException(String message, Object data) {
		return GenericResponse.builder().message(message).data(data).build();
	}

}

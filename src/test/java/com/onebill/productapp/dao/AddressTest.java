package com.onebill.productapp.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;

import com.onebill.productapp.dto.AddressBean;
import com.onebill.productapp.dto.CustomerBean;
import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.exception.CustomerNotFoundException;
import com.onebill.productapp.repository.AddressRepo;
import com.onebill.productapp.repository.CustomerRepo;
import com.onebill.productapp.service.impl.AddressServiceImpl;
import com.onebill.productapp.util.ResponseUtil;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AddressTest {

	private static final String CUSTOM_EXCEPTION_MESSAGE = "Customer with id '" + 0 + "' doesnt exists";

	@InjectMocks
	AddressServiceImpl service;

	@Mock
	AddressRepo repo;

	@Mock
	CustomerRepo customerRepo;

	static CustomerBean customerBean;

	static AddressBean bean;

	@BeforeAll
	public static void createProductObject() {
		bean = new AddressBean();
		bean.setAddress("Mg road");
		bean.setAddressId(1);
		bean.setAddressType("premanent");
		bean.setCity("Tumkur");
		bean.setCountry("india");
		bean.setPincode(572111);
		bean.setState("KAR");

		customerBean = new CustomerBean();
		customerBean.setCustomerId(2);
		customerBean.setActive(true);
		customerBean.setEmail("working@qas.com");
		customerBean.setFirstName("jack");
		customerBean.setLastName("k");
		customerBean.setPhone("+917766884512");

		customerBean.setAddressBean(Arrays.asList(bean));
	}

	// TestCase for addAddress service method
	@Test
	public void addAddressTest() {
		GenericResponse expected = ResponseUtil.fillerSuccess("address added successfully", bean);
		when(customerRepo.findById(customerBean.getCustomerId())).thenReturn(Optional.of(customerBean));
		GenericResponse actual = service.addAddress(customerBean.getCustomerId(), bean);
		assertEquals(expected, actual);
	}

	@Test
	public void addAddressFailureTest() {
		GenericResponse expected = ResponseUtil.fillerSuccess("Customer with id '" + 0 + "' doesnt exists", null);
		when(customerRepo.findById(0)).thenThrow(new CustomerNotFoundException(CUSTOM_EXCEPTION_MESSAGE));
		GenericResponse actual = null;
		try {
			service.addAddress(0, bean);
		} catch (Exception e) {
			actual = ResponseUtil.fillerException(e.getMessage(), null);
		}
		assertEquals(expected, actual);
	}

	@Test
	public void addAddressFailureTest1() {
		CustomerBean bean1 = new CustomerBean();
		GenericResponse expected = ResponseUtil.fillerSuccess("cid doesnt exist", null);
		when(customerRepo.findById(0)).thenReturn(Optional.of(bean1));
		GenericResponse actual = service.addAddress(bean1.getCustomerId(), null);
		;
		assertEquals(expected, actual);
	}

	@Test
	public void addAddressFailureTest2() {
		Assertions.assertThrows(CustomerNotFoundException.class,
				() -> repo.findById(0).orElseThrow(() -> new CustomerNotFoundException(CUSTOM_EXCEPTION_MESSAGE)));
	}

	// Test Case for getAddress service method
	@Test
	public void getAddressTest() {

		GenericResponse expected = ResponseUtil.fillerSuccess("address retrived successfully", customerBean);
		when(customerRepo.findById(customerBean.getCustomerId())).thenReturn(Optional.of(customerBean));
		GenericResponse actual = service.getAddress(customerBean.getCustomerId());
		assertEquals(expected, actual);
	}

	@Test
	public void getAddressExceptionTest() {
		Assertions.assertThrows(CustomerNotFoundException.class, () -> customerRepo.findById(0)
				.orElseThrow(() -> new CustomerNotFoundException(CUSTOM_EXCEPTION_MESSAGE)));
	}

	// TestCase for updateAddress service method
	@Test
	public void updateAddressTest() {
		AddressBean addbean = new AddressBean();
		addbean.setAddressId(bean.getAddressId());
		addbean.setAddressType("permanent");
		addbean.setState("mah");
		addbean.setCity("Mumbai");
		addbean.setCountry("india");
		addbean.setPincode(572112);
		GenericResponse expected = ResponseUtil.fillerSuccess("address updated successfully", bean);
		when(customerRepo.existsById(customerBean.getCustomerId())).thenReturn(true);
		when(repo.findById(bean.getAddressId())).thenReturn(Optional.of(addbean));
		when(customerRepo.findById(customerBean.getCustomerId())).thenReturn(Optional.of(customerBean));
		when(repo.save(addbean)).thenReturn(addbean);
		GenericResponse actual = service.updateAddress(customerBean.getCustomerId(), bean);
		assertEquals(expected, actual);
	}

	@Test
	public void updateAddressFailureTest() {
		AddressBean addbean = new AddressBean();
		addbean.setAddressId(bean.getAddressId());
		addbean.setAddressType("permanent");
		addbean.setState("mah");
		addbean.setCity("Mumbai");
		addbean.setCountry("india");
		addbean.setPincode(572112);
		GenericResponse expected = ResponseUtil.fillerSuccess("cid doesnt exist", null);
		when(customerRepo.existsById(0)).thenReturn(false);
		GenericResponse actual = service.updateAddress(0, bean);
		assertEquals(expected, actual);
	}
	

	@Test
	public void updateAddressExceptionTest() {
		Assertions.assertThrows(CustomerNotFoundException.class, () -> repo.findById(0)
				.orElseThrow(() -> new CustomerNotFoundException(CUSTOM_EXCEPTION_MESSAGE)));
	}

	//TestCase for deleteAddress service method
	@Test
	public void deleteAddressTest() {
		GenericResponse expected = ResponseUtil.fillerSuccess("address deleted successfully", bean.getAddressId());
		when(customerRepo.existsById(customerBean.getCustomerId())).thenReturn(true);
		when(repo.existsById(bean.getAddressId())).thenReturn(true);
		
		GenericResponse actual = service.deleteAddress(customerBean.getCustomerId(), bean.getAddressId());
		assertEquals(expected, actual);
	}
	
	@Test
	public void deleteAddressFailureTest() {
		GenericResponse expected = ResponseUtil.fillerSuccess("id doesnt exists",null);
		when(customerRepo.existsById(customerBean.getCustomerId())).thenReturn(false);
		when(repo.existsById(bean.getAddressId())).thenReturn(false);
		
		GenericResponse actual = service.deleteAddress(customerBean.getCustomerId(), bean.getAddressId());
		assertEquals(expected, actual);
	}
	
	@Test
	public void deleteAddressExceptionTest() {
		Assertions.assertThrows(CustomerNotFoundException.class, () -> repo.findById(0)
				.orElseThrow(() -> new CustomerNotFoundException(CUSTOM_EXCEPTION_MESSAGE)));
	}
}

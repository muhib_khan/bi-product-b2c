package com.onebill.productapp.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;

import com.onebill.productapp.dto.CustomerBean;
import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.exception.CustomerNotFoundException;
import com.onebill.productapp.repository.CustomerRepo;
import com.onebill.productapp.service.impl.CustomerServiceImpl;
import com.onebill.productapp.util.ResponseUtil;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CustomerDaoImplTest {

	private static final String CUSTOM_EXCEPTION_MESSAGE = "Customer with id '" + 0 + "' doesnt exists";

	@InjectMocks
	CustomerServiceImpl service;

	@Mock
	CustomerRepo repo;

	static CustomerBean bean;

	@BeforeAll
	public static void createProductObject() {
		bean = new CustomerBean();
		bean.setCustomerId(2);
		bean.setActive(true);
		bean.setEmail("working@qas.com");
		bean.setFirstName("jack");
		bean.setLastName("k");
		bean.setPhone("+917766884512");

	}

// TestCase for addCustomer service method
	@Test
	public void addCustomerTest() {
		GenericResponse expected = ResponseUtil.fillerSuccess("customer added successfully", bean);
		when(repo.save(bean)).thenReturn(bean);
		GenericResponse actual = service.addCustomer(bean);
		assertEquals(expected, actual);
	}

	@Test
	public void addCustomerTestFailure() {
		GenericResponse expected = ResponseUtil.fillerFailure("customer email already exist", null);
		when(repo.existsByEmail(bean.getEmail())).thenReturn(true);
//		when(service.addCustomer(bean)).thenReturn(expected);
		GenericResponse actual = service.addCustomer(bean);
		assertEquals(expected, actual);
	}

	@Test
	public void addCustomerTestException() {
		GenericResponse expected = ResponseUtil.fillerException("Exception occured :-", null);
		when(repo.existsByEmail(bean.getEmail())).thenThrow(NullPointerException.class);
//		when(repo.save(bean)).thenReturn(null);
		GenericResponse actual = service.addCustomer(bean);
		assertEquals(expected, actual);
	}

// TestCase for updateCustomer service method
	@Test
	public void updateCustomerTest() {
		CustomerBean updateBean = new CustomerBean();
		updateBean.setActive(bean.isActive());
		updateBean.setCustomerId(bean.getCustomerId());
		updateBean.setCustomerType(bean.getCustomerType());
		updateBean.setEmail(bean.getEmail());
		updateBean.setFirstName("mohib");
		updateBean.setLastName("Khan");
		updateBean.setPhone(bean.getPhone());
		GenericResponse expected = ResponseUtil.fillerSuccess("customer updated successfully", updateBean);
		when(repo.existsById(bean.getCustomerId())).thenReturn(true);

		when(repo.findById(2)).thenReturn(Optional.of(bean));
		when(repo.save(bean)).thenReturn(updateBean);
		GenericResponse actual = service.updateCustomer(bean);
		assertEquals(expected, actual);
	}

	@Test
	public void updateCustomerTestFailure() {
		GenericResponse expected = ResponseUtil.fillerFailure("data not found for registered Customer ", null);
		when(repo.existsById(bean.getCustomerId())).thenReturn(false);
		GenericResponse actual = service.updateCustomer(bean);
		assertEquals(expected, actual);
	}

	@Test
	public void updateCustomerTestException() {
		GenericResponse expected = ResponseUtil.fillerFailure("Exception occured :-", null);
		when(repo.existsById(bean.getCustomerId())).thenReturn(true);
		GenericResponse actual = service.updateCustomer(bean);
		assertEquals(expected, actual);
		Assertions.assertThrows(RuntimeException.class, () -> repo.findById(null).orElseThrow(RuntimeException::new));
	}

//	TestCase for deleteCustomer service method
	@Test
	public void deleteCustomerTest() {
		bean.setActive(false);
		GenericResponse expected = ResponseUtil.fillerSuccess("customer deleted successfully", bean);
		when(repo.findById(bean.getCustomerId())).thenReturn(Optional.of(bean));
		when(repo.save(bean)).thenReturn(bean);
		GenericResponse actual = service.deleteCustomer(bean.getCustomerId());
		assertEquals(expected, actual);
	}

	@Test
	public void deleteCustomerExceptionTest() {
		when(repo.findById(0)).thenThrow(CustomerNotFoundException.class);
		Assertions.assertThrows(CustomerNotFoundException.class, () -> repo.findById(0));
	}

	@Test
	public void deleteCustomerCustomExceptionTest() {
		Assertions.assertThrows(CustomerNotFoundException.class, () -> repo.findById(0)
				.orElseThrow(() -> new CustomerNotFoundException(CUSTOM_EXCEPTION_MESSAGE)));
	}

	@Test
	public void deleteCustomerServiceCustomExceptionTest() {
		
		GenericResponse expected = ResponseUtil.fillerException(CUSTOM_EXCEPTION_MESSAGE, null);
		when(repo.findById(0)).thenThrow(new CustomerNotFoundException(CUSTOM_EXCEPTION_MESSAGE));
		GenericResponse actual =null; ;
		try {
			service.deleteCustomer(0);
		} catch (Exception e) {
			actual = ResponseUtil.fillerException(e.getMessage(), null);
		}
		assertEquals(expected, actual);
	}
	
	// Test case for getActiveCustomer service method
	@Test
	public void getActiveCustomerTest() {
		List<CustomerBean> list=new ArrayList<>();
		list.add(bean);
		GenericResponse expected = ResponseUtil.fillerSuccess("All Active Customers found", list);
		when(repo.findByIsActiveIs(true)).thenReturn(list);
		GenericResponse actual = service.getActiveCustomer(true);
		assertEquals(expected, actual);
	}

	@Test
	public void getInActiveCustomerTest() {
		CustomerBean customerBean=new CustomerBean();
		customerBean.setActive(false);
		List<CustomerBean> list=new ArrayList<>();
		list.add(customerBean);
		GenericResponse expected = ResponseUtil.fillerSuccess("All Active Customers found", list);
		when(repo.findByIsActiveIs(false)).thenReturn(list);
		GenericResponse actual = service.getActiveCustomer(false);
		assertEquals(expected, actual);
	}
	
	@Test
	public void getActiveCustomerFailureTest() {
		List<CustomerBean> list=new ArrayList<>();
		GenericResponse expected = ResponseUtil.fillerSuccess("No Active Customers found", null);
		when(repo.findByIsActiveIs(true)).thenReturn(list);
		GenericResponse actual = service.getActiveCustomer(true);
		assertEquals(expected, actual);
	}
	
	@Test
	public void getActiveCustomerExceptionTest() {
		GenericResponse expected = ResponseUtil.fillerSuccess("Exception occured :-", null);
		when(repo.findByIsActiveIs(true)).thenReturn(null);
		GenericResponse actual = service.getActiveCustomer(true);
		assertEquals(expected, actual);
	}
	
	// TestCase for getAllCustomer service method
	@Test
	public void getAllCustomerSuccessTest() {
		List<CustomerBean> list=new ArrayList<>();
		list.add(bean);
		GenericResponse expected = ResponseUtil.fillerSuccess("Customers found", list);
		when(repo.findAll()).thenReturn(list);
		GenericResponse actual = service.getAllCustomer();
		assertEquals(expected, actual);
	}
	
	@Test
	public void getAllCustomerExceptionTest() {
		GenericResponse expected = ResponseUtil.fillerException("Customers found", null);
		when(repo.findAll()).thenReturn(null);
		GenericResponse actual = service.getAllCustomer();
		assertEquals(expected, actual);
	}

}

package com.onebill.productapp.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.dto.ProductBean;
import com.onebill.productapp.exception.ProductNotFoundExecption;
import com.onebill.productapp.repository.ProductRepo;
import com.onebill.productapp.service.impl.ProductServiceImpl;
import com.onebill.productapp.util.ResponseUtil;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ProductDaoImplTest {
	
	private static final String PRODUCT_EXCEPTION_MESSAGE = "Product with id '" + 2 + "' doesnt exists";

	//inject the mock into service class
	@InjectMocks
	ProductServiceImpl serviceImpl;

	//mock the Spring data jpa repository
	@Mock
	ProductRepo productRepo;

	//product object
	static ProductBean bean;


	//initialize the product object before all test
	@BeforeAll
	public static void createProductObject() {
		bean = new ProductBean();
		bean.setActive(true);
		bean.setDescription("oneplus nord 2020");
		bean.setPrice(30000);
		bean.setProductCode("ONE111");
		bean.setProductName("oneplus nord");
		bean.setQuantity(12);
		bean.setRating(4);
		bean.setProductId(2);

	}

	// TestCase for addProduct service method
	@Test
	void addProductTest() {
		GenericResponse expected = ResponseUtil.fillerSuccess("product added successfully", bean);
		when(productRepo.save(bean)).thenReturn(bean);
		GenericResponse actual = serviceImpl.addProduct(bean);
		assertEquals(expected, actual);
	}

	@Test
	void addExistProductTest() {
		GenericResponse expected = ResponseUtil.fillerSuccess("product Code is not unique", null);
		when(productRepo.existsByProductCode(bean.getProductCode())).thenReturn(true);
		GenericResponse actual = serviceImpl.addProduct(bean);
		assertEquals(expected, actual);
	}
	@Test
	void addProductExceptionTest() {
		GenericResponse expected = ResponseUtil.fillerFailure("Exception occured :-", null);
		when(productRepo.existsByProductCode(bean.getProductCode())).thenThrow(NullPointerException.class);
		GenericResponse actual = serviceImpl.addProduct(bean);
		assertEquals(expected, actual);
	}
	// TestCase for updateProduct service method
	@Test
	void updateProductTest() {

		ProductBean updateProduct = new ProductBean();
		updateProduct.setDescription(bean.getDescription());
		updateProduct.setPrice(bean.getPrice());
		updateProduct.setProductCode(bean.getProductCode());
		updateProduct.setProductId(bean.getProductId());
		updateProduct.setProductName("Mobile");
		updateProduct.setQuantity(bean.getQuantity());
		updateProduct.setRating(bean.getRating());
		updateProduct.setActive(true);
		GenericResponse expected = ResponseUtil.fillerException("product updated successfully", updateProduct);
		when(productRepo.existsById(bean.getProductId())).thenReturn(true);

		when(productRepo.findById(2)).thenReturn(Optional.of(bean));
		when(productRepo.save(bean)).thenReturn(updateProduct);
		GenericResponse actual = serviceImpl.editProduct(bean);
		assertEquals(expected, actual);
	}
	
	@Test
	void updateProductNotFoundTest() {

		GenericResponse expected = ResponseUtil.fillerFailure("product not found", null);
		when(productRepo.existsById(bean.getProductId())).thenReturn(false);

		GenericResponse actual = serviceImpl.editProduct(bean);
		assertEquals(expected, actual);
	}
	@Test
	void updateProductExceptionTest() {
		GenericResponse expected = ResponseUtil.fillerFailure("Exception occured :-", null);
		when(productRepo.existsById(bean.getProductId())).thenReturn(true);
		when(productRepo.findById(2)).thenThrow(ProductNotFoundExecption.class);
		GenericResponse actual = serviceImpl.editProduct(bean);
		assertEquals(expected, actual);
		Assertions.assertThrows(ProductNotFoundExecption.class, () -> productRepo.findById(2).orElseThrow(()->
		new ProductNotFoundExecption("Product with id " + bean.getProductId() + " doesnt exists")));
	}
	// TestCase for deleteProduct service method
	@Test
	void deleteProductTest() {
		bean.setActive(false);
		GenericResponse expected = ResponseUtil.fillerSuccess("product deleted successfully", null);
		when(productRepo.existsById(bean.getProductId())).thenReturn(true);
		when(productRepo.findById(bean.getProductId())).thenReturn(Optional.of(bean));
		when(productRepo.save(bean)).thenReturn(bean);
		GenericResponse actual = serviceImpl.deleteProduct(bean.getProductId());
		assertEquals(expected, actual);
	}
	@Test
	void deleteProductNotFoundTest() {
		GenericResponse expected = ResponseUtil.fillerFailure("product not found", null);
		when(productRepo.existsById(bean.getProductId())).thenReturn(false);
		GenericResponse actual = serviceImpl.deleteProduct(bean.getProductId());
		assertEquals(expected, actual);
	}
	@Test
	void deleteProductExeceptionTest() {
		when(productRepo.existsById(bean.getProductId())).thenReturn(true);
		when(productRepo.findById(null)).thenThrow(ProductNotFoundExecption.class);
		Assertions.assertThrows(ProductNotFoundExecption.class, () -> productRepo.findById(null));
	}
	@Test
	void deleteProductCustomExeceptionTest() {
		Assertions.assertThrows(ProductNotFoundExecption.class, () -> 
		productRepo.findById(null).orElseThrow(()-> new ProductNotFoundExecption(PRODUCT_EXCEPTION_MESSAGE)));
	}
	// TestCase for getActiveProduct service method
	@Test
	public void getActiveProductTest() {
		bean.setActive(true);
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "productName"));
		List<ProductBean> list=new ArrayList<>();
		list.add(bean);
		GenericResponse expected = ResponseUtil.fillerSuccess("All Active product found", list);
		when(productRepo.findByIsActiveIs(true, pageable)).thenReturn(list);
		GenericResponse actual = serviceImpl.getActiveProduct(true);
		assertEquals(expected, actual);
	}
	@Test
	public void getInActiveProductTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "productName"));
		ProductBean productBean=new ProductBean();
		productBean.setActive(false);
		List<ProductBean> list=new ArrayList<>();
		list.add(productBean);
		GenericResponse expected = ResponseUtil.fillerSuccess("All Active product found", list);
		when(productRepo.findByIsActiveIs(false,pageable)).thenReturn(list);
		GenericResponse actual = serviceImpl.getActiveProduct(false);
		assertEquals(expected, actual);
	}
	@Test
	public void getActiveProductFailureTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "productName"));
		List<ProductBean> list=new ArrayList<>();
		GenericResponse expected = ResponseUtil.fillerSuccess("No Active product found", null);
		when(productRepo.findByIsActiveIs(true,pageable)).thenReturn(list);
		GenericResponse actual = serviceImpl.getActiveProduct(true);
		assertEquals(expected, actual);
	}
	@Test
	public void getActiveProductExceptionTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "productName"));
		GenericResponse expected = ResponseUtil.fillerSuccess("Exception occured :-", null);
		when(productRepo.findByIsActiveIs(true,pageable)).thenReturn(null);
		GenericResponse actual = serviceImpl.getActiveProduct(true);
		assertEquals(expected, actual);
	}
	// TestCase for getAllProduct service method
	@Test
	public void getAllProductTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "productName"));
		List<ProductBean> list=new ArrayList<>();
		list.add(bean);
		Page<ProductBean> pagedTasks = new PageImpl<>(list);
		GenericResponse expected = ResponseUtil.fillerSuccess("Product found", list);
		when(productRepo.findAll(pageable)).thenReturn(pagedTasks);
		GenericResponse actual = serviceImpl.getAllProduct();
		assertEquals(expected, actual);
	}
	@Test
	public void getAllProductExeceptionTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "productName"));
		GenericResponse expected = ResponseUtil.fillerSuccess("Exception occured :-", null);
		when(productRepo.findAll(pageable)).thenReturn(null);
		GenericResponse actual = serviceImpl.getAllProduct();
		assertEquals(expected, actual);
	}
	// TestCase for getProductByPriceRange service method
	@Test
	public void getProductByPriceRangeTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "price"));
		List<ProductBean> list=new ArrayList<>();
		list.add(bean);
		GenericResponse expected = ResponseUtil.fillerSuccess("product found Range", list);
		when(productRepo.findByPriceBetweenAndIsActiveTrue(1, 50000, pageable)).thenReturn(list);
		GenericResponse actual = serviceImpl.getProductByPriceRange(1, 50000);
		assertEquals(expected, actual);
	}
	@Test
	public void getProductByPriceRangeNotExixstTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "price"));
		List<ProductBean> list = new ArrayList<>();
		GenericResponse expected = ResponseUtil.fillerFailure("product not in a found", null);
		when(productRepo.findByPriceBetweenAndIsActiveTrue(0, 500, pageable)).thenReturn(list);
		GenericResponse actual = serviceImpl.getProductByPriceRange(0, 500);
		assertEquals(expected, actual);
	}
	@Test
	public void getProductByPriceRangeExeceptionTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "price"));
		GenericResponse expected = ResponseUtil.fillerException("Exception occured :-", null);
		when(productRepo.findByPriceBetweenAndIsActiveTrue(0, 0, pageable)).thenReturn(null);
		GenericResponse actual = serviceImpl.getProductByPriceRange(0, 0);
		assertEquals(expected, actual);
	}
	@Test
	public void getProductByRatingRangeTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "rating"));
		List<ProductBean> list=new ArrayList<>();
		list.add(bean);
		GenericResponse expected = ResponseUtil.fillerSuccess("product found Range", list);
		when(productRepo.findByRatingBetweenAndIsActiveTrue(1, 5, pageable)).thenReturn(list);
		GenericResponse actual = serviceImpl.getProductByRatingRange(1, 5);
		assertEquals(expected, actual);
	}
	// TestCase for getProductByRatingRange service method
	@Test
	public void getProductByRatingRangeNotExixstTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "rating"));
		List<ProductBean> list = new ArrayList<>();
		GenericResponse expected = ResponseUtil.fillerFailure("product not in a found", null);
		when(productRepo.findByRatingBetweenAndIsActiveTrue(0, 500, pageable)).thenReturn(list);
		GenericResponse actual = serviceImpl.getProductByRatingRange(0, 500);
		assertEquals(expected, actual);
	}
	@Test
	public void getProductByRatingRangeExeceptionTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "rating"));
		GenericResponse expected = ResponseUtil.fillerFailure("Exception occured :-", null);
		when(productRepo.findByRatingBetweenAndIsActiveTrue(0, 0, pageable)).thenReturn(null);
		GenericResponse actual = serviceImpl.getProductByRatingRange(0, 0);
		assertEquals(expected, actual);
	}
	// TestCase for getProductByNameLike service method
	@Test
	public void getProductByNameLikeTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "productName"));
		List<ProductBean> list=new ArrayList<>();
		list.add(bean);
		GenericResponse expected = ResponseUtil.fillerSuccess("product found Range", list);
		when(productRepo.findByProductNameStartingWithAndIsActiveTrue("one", pageable)).thenReturn(list);
		GenericResponse actual = serviceImpl.getProductByNameLike("one");
		assertEquals(expected, actual);
	}
	@Test
	public void getProductByNameLikeNotExixstTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "productName"));
		List<ProductBean> list = new ArrayList<>();
		GenericResponse expected = ResponseUtil.fillerFailure("product not in a found", null);
		when(productRepo.findByProductNameStartingWithAndIsActiveTrue("", pageable)).thenReturn(list);
		GenericResponse actual = serviceImpl.getProductByNameLike("");
		assertEquals(expected, actual);
	}
	@Test
	public void getProductByNameLikeExeceptionTest() {
		Pageable pageable = PageRequest.of(0, 5,  Sort.by(Sort.Direction.ASC, "productName"));
		GenericResponse expected = ResponseUtil.fillerFailure("Exception occured :-", null);
		when(productRepo.findByProductNameStartingWithAndIsActiveTrue("", pageable)).thenReturn(null);
		GenericResponse actual = serviceImpl.getProductByNameLike("");
		assertEquals(expected, actual);
	}
	

}

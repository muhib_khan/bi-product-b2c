package com.onebill.productapp.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.onebill.productapp.dto.GenericResponse;
import com.onebill.productapp.dto.ProductBean;
import com.onebill.productapp.repository.ProductRepo;
import com.onebill.productapp.service.impl.ProductServiceImpl;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class ProductServiceTest {
	
	@InjectMocks
	ProductServiceImpl serviceImpl;
	
	@Mock
	ProductRepo  mock;
	
	@Before
	void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	void testAddProductWithInvalidData() {
		ProductBean bean = new ProductBean();
		bean.setActive(true);
		bean.setDescription("");
		bean.setPrice(55.5);
		bean.setProductCode("jjjkk");
		bean.setProductName("");
		bean.setQuantity(0);
		bean.setRating(0);
		
		  GenericResponse expected = GenericResponse.builder().message("Exception occured").data(null).build();
		when(mock.save(bean)).thenReturn(bean);
		GenericResponse actual = serviceImpl.addProduct(bean);
		assertEquals(expected, actual);
		
	}

}
